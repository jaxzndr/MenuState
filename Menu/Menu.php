<?php
/**
 * 

 */

namespace Menu;

class Menu
{
    private $state;
    
    function getState()
    {
        return $this->state;
    }

    function setState(MenuStateInterface $state)
    {
        $this->state = $state;
        
    }
    
    public function __construct()
    {
        $this->setState(new \Menu\State\Facebook());
        
    }

    public function facebook()
    {
        $this->setState($this->state->facebook());
    }

    public function twitter()
    {

        $this->setState($this->state->twitter());
    }

    public function youtube()
    {
        $this->setState($this->state->youtube());
    }

    public function linkedin()
    {
        $this->setState($this->state->linkedin());
    }

    public function instagram()
    {
        $this->setState($this->state->instagram());
    }


    
   

}
