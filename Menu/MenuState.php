<?php
/**
 * 
 *
 */

namespace Menu;

class MenuState implements MenuStateInterface
{
    public function facebook()
    {
        throw new \Menu\Exception\IllegalStateTransitionException();
    }

    public function twitter()
    {
        throw new \Menu\Exception\IllegalStateTransitionException();
    }

    public function youtube()
    {
        throw new \Menu\Exception\IllegalStateTransitionException();
    }
    
    public function linkedin()
    {
        throw new \Menu\Exception\IllegalStateTransitionException();
    }
    
    public function instagram()
    {
        throw new \Menu\Exception\IllegalStateTransitionException();
    }

}
