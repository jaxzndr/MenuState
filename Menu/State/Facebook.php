<?php

namespace Menu\State;

	class Facebook extends \Menu\MenuState
	{
		public function facebook()
		{
			
			return new Facebook();
		}

		public function twitter()
		{
			return new Twitter();
		}

		public function youtube()
		{
			return new Youtube();
		}

		public function linkedin()
		{
			return new Linkedin();
		}

		public function instagram()
		{
			return new Instagram();
		}

	}


?>

