<?php

namespace Menu\State;

	class Instagram extends \Menu\MenuState
	{
		public function instagram ()
		{
			return new Instagram();
		}

		public function facebook()
		{
			return new Facebook();
		}

		public function twitter()
		{
			return new Twitter();
		}

		public function youtube()
		{
			return new Youtube();
		}

		public function linkedin ()
		{
			return new Linkedin();
		}

		

	}


?>

