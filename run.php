<div class="content">
    

    
    <h3>Menu</h3>
	<br>
	<script language=JavaScript>

	<?php 
    require_once 'autoload.php';
	$menu = new Menu\Menu();
	$menu->twitter();
	$menu->youtube();
	$menu->linkedin();	
	$menu->instagram();
    ?>
		var numeroClicks1 = 10;
		var numeroClicks2 = 10;
		var numeroClicks3 = 10;
		var numeroClicks4 = 10;
		var numeroClicks5 = 10;

		function contador_onclick1() {
		   numeroClicks1++;
		   window.document.form1.ButtonFacebook.value = 'Botón Facebook pulsado ' + numeroClicks1 + 
		   ' veces';

		   if(numeroClicks1==15){
		   		document.getElementById('ButtonFacebookPref').style.display = 'block';
		   		document.getElementById('ButtonFacebook').style.display = 'none';
		   	}
		}

		function contador_onclick2() {
		   numeroClicks2++;
		   window.document.form2.ButtonTwitter.value = 'Botón Twitter pulsado ' + numeroClicks2 + 
		   ' veces';

		   if(numeroClicks2==15){
		   		document.getElementById('ButtonTwitterPref').style.display = 'block';
		   		document.getElementById('ButtonTwitter').style.display = 'none';
		   	}
		}

		function contador_onclick3() {
		   numeroClicks3++;
		   window.document.form3.ButtonYoutube.value = 'Botón Youtube pulsado ' + numeroClicks3 + 
		   ' veces';

		   if(numeroClicks3==15){
		   		document.getElementById('ButtonYoutubePref').style.display = 'block';
		   		document.getElementById('ButtonYoutube').style.display = 'none';
		   	}
		}

		function contador_onclick4() {
		   numeroClicks4++;
		   window.document.form4.ButtonLinkedIn.value = 'Botón LinkedIn pulsado ' + numeroClicks4 + 
		   ' veces';

		   if(numeroClicks4==15){
		   		document.getElementById('ButtonLinkedInPref').style.display = 'block';
		   		document.getElementById('ButtonLinkedIn').style.display = 'none';
		   	}
		}

		function contador_onclick5() {
		   numeroClicks5++;
		   window.document.form5.ButtonInstagram.value = 'Botón Instagram pulsado ' + numeroClicks5 + 
		   ' veces';

		   if(numeroClicks5==15){
		   		document.getElementById('ButtonInstagramPref').style.display = 'block';
		   		document.getElementById('ButtonInstagram').style.display = 'none';
		   	}
		}

		</script>

	</script>
	<br>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<style>
	.w3-button {width:280px;}
	</style>
	
	<div class="w3-container">

		<form id="ButtonFacebookPref" style='display:none;'>
			<INPUT TYPE='button' class="w3-button w3-teal" NAME='Button' VALUE='Facebook'>
		</form>

		<form id="ButtonTwitterPref" style='display:none;'>
			<INPUT TYPE='button' class="w3-button w3-green" NAME='Button' VALUE='Twitter'>
		</form>

		<form id="ButtonYoutubePref" style='display:none;'>
			<INPUT TYPE='button' class="w3-button w3-light-green" NAME='Button' VALUE='Youtube'>
		</form>

		<form id="ButtonLinkedInPref" style='display:none;'>
			<INPUT TYPE='button' class="w3-button w3-lime" NAME='Button' VALUE='LinkedIn'>
		</form>

		<form id="ButtonInstagramPref" style='display:none;'>
			<INPUT TYPE='button' class="w3-button w3-sand" NAME='Button' VALUE='Instagram'>
		</form>

		<FORM id="ButtonFacebook" NAME=form1 style='display:block;'>
		   <INPUT TYPE='button' class="w3-button w3-blue" NAME='ButtonFacebook' VALUE='Facebook' 
		   onclick="contador_onclick1()">
		</FORM>

		<FORM id="ButtonTwitter" NAME=form2 style='display:block;'>
		   <INPUT TYPE='button' class="w3-button w3-blue" NAME='ButtonTwitter' VALUE='Twitter' 
		   onclick="contador_onclick2()">
		</FORM>

		<FORM id="ButtonYoutube" NAME=form3 style='display:block;'>
		   <INPUT TYPE='button' class="w3-button w3-blue" NAME='ButtonYoutube' VALUE='Youtube' 
		   onclick="contador_onclick3()">
		</FORM>

		<FORM id="ButtonLinkedIn" NAME=form4 style='display:block;'>
		   <INPUT TYPE='button' class="w3-button w3-blue" NAME='ButtonLinkedIn' VALUE='LinkedIn' 
		   onclick="contador_onclick4()">
		</FORM>

		<FORM id="ButtonInstagram" NAME=form5 style='display:block;'>
		   <INPUT TYPE='button' class="w3-button w3-blue" NAME='ButtonInstagram' VALUE='Instagram' 
		   onclick="contador_onclick5()">
		</FORM>
	</div>
</div>